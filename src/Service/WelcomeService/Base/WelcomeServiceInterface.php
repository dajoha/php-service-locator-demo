<?php

declare(strict_types=1);

namespace App\Service\WelcomeService\Base;

interface WelcomeServiceInterface
{
    public function getMessage(): string;
}
