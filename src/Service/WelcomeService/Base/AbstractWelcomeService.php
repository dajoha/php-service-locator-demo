<?php

declare(strict_types=1);

namespace App\Service\WelcomeService\Base;

use App\Misc\HtmlRenderer;

abstract class AbstractWelcomeService implements WelcomeServiceInterface
{
    public function __construct()
    {
        $class = static::class;
        HtmlRenderer::addLogMessage("Constructor was called for welcome service class <u>$class</u>");
    }
}
