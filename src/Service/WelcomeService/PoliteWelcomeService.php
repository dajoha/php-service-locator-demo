<?php

declare(strict_types=1);

namespace App\Service\WelcomeService;

use App\Service\WelcomeService\Base\AbstractWelcomeService;

class PoliteWelcomeService extends AbstractWelcomeService
{
    public function getMessage(): string
    {
        return "Hello, sir";
    }
}
