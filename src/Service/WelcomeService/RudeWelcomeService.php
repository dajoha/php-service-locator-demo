<?php

declare(strict_types=1);

namespace App\Service\WelcomeService;

use App\Service\WelcomeService\Base\AbstractWelcomeService;

class RudeWelcomeService extends AbstractWelcomeService
{
    public function getMessage(): string
    {
        return "Go to hell";
    }
}
