<?php

declare(strict_types=1);

namespace App\DependencyInjection\CompilerPass;

use Symfony\Component\DependencyInjection\Argument\ServiceClosureArgument;
use Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Definition;
use Symfony\Component\DependencyInjection\Reference;
use Symfony\Component\DependencyInjection\ServiceLocator;

class WelcomeServiceLocatorPass implements CompilerPassInterface
{
    public function process(ContainerBuilder $container)
    {
        $taggedWelcomeServices = $container->findTaggedServiceIds('app.service.welcome');

        $welcomeServices = [];
        foreach ($taggedWelcomeServices as $fqcn => $welcomeService) {
            $kebabName = self::fqcnToKebabName($fqcn);
            $welcomeServices[$kebabName] = new ServiceClosureArgument(new Reference($fqcn));
        }

        $locator = (new Definition(ServiceLocator::class))->addArgument($welcomeServices);
        $container->setDefinition('welcome.service_locator', $locator);
    }

    /**
     * Transforms something like "One\Two\Three\FooBarWelcomeService" to "foo-bar".
     */
    protected static function fqcnToKebabName(string $fqcn): ?string
    {
        $fqcnParts = explode('\\', $fqcn);
        $classname = $fqcnParts[count($fqcnParts) - 1];
        preg_match('/(.*)WelcomeService$/', $classname, $matches);
        $prefix = $matches[1] ?? null;
        if (null !== $prefix) {
            $kebabName = strtolower(preg_replace('/(?<!^)[A-Z]/', '-$0', $prefix));
        }

        return empty($kebabName) ? null : $kebabName;
    }
}
