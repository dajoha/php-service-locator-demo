<?php

declare(strict_types=1);

namespace App\Controller\Base;

use App\Misc\HtmlRenderer;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController as BaseAbstractController;

class AbstractController extends BaseAbstractController
{
    public function addControllerLogMessage(string $method): void
    {
        HtmlRenderer::addLogMessage("Controller: <u>$method</u>");
    }
}
