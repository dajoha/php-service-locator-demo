<?php

declare(strict_types=1);

namespace App\Controller;

use App\Controller\Base\AbstractController;
use App\Misc\HtmlRenderer;
use Symfony\Component\DependencyInjection\ServiceLocator;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class HomeController extends AbstractController
{
    #[Route]
    public function home(ServiceLocator $welcomeServiceLocator): Response
    {
        $this->addControllerLogMessage(__METHOD__);

        $welcomeServiceAliases = array_keys($welcomeServiceLocator->getProvidedServices());

        $links = array_map(
            fn($alias) => "<li><a href='/$alias'>/$alias</a></li>",
            $welcomeServiceAliases,
        );
        $links = implode("\n", $links);

        return HtmlRenderer::render(<<<HTML
            <h4>Welcome routes</h4>
            <ul>$links</ul>
            HTML);
    }
}
