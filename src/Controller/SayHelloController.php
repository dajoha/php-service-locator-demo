<?php

declare(strict_types=1);

namespace App\Controller;

use App\Controller\Base\AbstractController;
use App\Misc\HtmlRenderer;
use Psr\Container\ContainerExceptionInterface;
use Psr\Container\NotFoundExceptionInterface;
use Symfony\Component\DependencyInjection\ServiceLocator;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Annotation\Route;

class SayHelloController extends AbstractController
{
    /**
     * @throws ContainerExceptionInterface
     * @throws NotFoundExceptionInterface
     */
    #[Route('/{welcomeServiceAlias}', requirements: ['welcomeServiceAlias' => '^[a-z-]+$'])]
    public function sayHello(
        string $welcomeServiceAlias,
        ServiceLocator $welcomeServiceLocator,
    ): Response {
        $this->addControllerLogMessage(__METHOD__);

        $welcomeService = $welcomeServiceLocator->get($welcomeServiceAlias);

        if (null === $welcomeService) {
            throw new NotFoundHttpException();
        }

        return HtmlRenderer::render($welcomeService->getMessage());
    }
}
