<?php

declare(strict_types=1);

namespace App\Misc;

use Symfony\Component\HttpFoundation\Response;

class HtmlRenderer
{
    public const HTML_TEMPLATE = <<<HTML
        <!doctype html>
        <html lang="fr">
        
        <head>
            <meta charset="utf-8">
            <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
        
            <title>Test</title>
        
            <style>
                html, body {
                    color: ivory;
                    background-color: #222;
                }
                a {
                    color: darkorange;
                    text-decoration: none;
                    font-family: monospace;                
                }
                li {
                    list-style: none;
                }
            </style>
        </head>
        
        <body>
            <a href="/">[Home]</a>
            <hr>
            {{LOG_MESSAGES}}
            {{CONTENT}}
        </body>
        
        </html>
        HTML;

    static array $logMessages = [];

    public static function render(string $htmlContent): Response
    {
        $variables = [
            '{{LOG_MESSAGES}}' => self::getHtmlFormattedLogMessages(),
            '{{CONTENT}}' => $htmlContent,
        ];
        $body = str_replace(array_keys($variables), array_values($variables), self::HTML_TEMPLATE);

        return new Response($body);
    }

    public static function addLogMessage(string $message): void
    {
        self::$logMessages[] = $message;
    }

    protected static function getHtmlFormattedLogMessages(): string
    {
        $messages = implode("\n", self::$logMessages);

        return <<<HTML
            <pre>
            <b>Logged messages:</b>
            $messages
            </pre>
            <hr>
            HTML;
    }
}
